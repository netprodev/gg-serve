#!/usr/bin/env python

import select
import socket
import sys
import os
import base64
import threading

# Base HTTP Server
class Server:
    def __init__(self):
        self.host = 'localhost'
        self.port = 8888
        self.backlog = 5
        self.size = 1024
        self.server = None
        self.threads = []

    def open_socket(self):        
        self.server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.server.bind((self.host,self.port))
        self.server.listen(5)
        
    def run(self):
        self.open_socket()
        input = [self.server]
        running = 1
        while running:
            inputready,outputready,exceptready = select.select(input,[],[])

            for s in inputready:

                if s == self.server:
                    # handle the server socket
                    c = Client(self.server.accept())
                    c.start()
                    self.threads.append(c)

                elif s == sys.stdin:
                    # handle standard input
                    junk = sys.stdin.readline()
                    running = 0

        # close all threads

        self.server.close()
        for c in self.threads:
            c.join()

# Multiclient Thread handler
class Client(threading.Thread):
    def __init__(self,(client,address)):
        threading.Thread.__init__(self)
        self.client = client
        self.address = address
        self.size = 1024

    def run(self):
        try:
            handler = requestHandler(self.client)
            handler.set_app(statusHandler)
            handler.handle_request()
        except:
            response = "HTTP:/1.1 500 internal server error\r\n"
            response += "Server: GG Serve\r\n"
            response += "Host: localhost\r\n"
            response += "Content-Type: text/html\r\n"
            response += "\r\n"

            response += getFileContents("error_handler/500.html")

            self.client.sendall(response)
            self.client.close()


#HTTP Request Handler
class requestHandler(object):
    
    def __init__(self, client_connection):
        self.client_connection = client_connection
        self.headers_set = []

    def set_app(self, application):
        self.application = application

    def handle_request(self):
        self.request_data = request_data = self.client_connection.recv(1024)
        # Print formatted request data 
        print(''.join(
            '< {line}\n'.format(line=line)
            for line in request_data.splitlines()
        ))
        if (request_data != ''):
            self.parse_request(request_data)

            # Construct environment dictionary using request data
            env = self.get_environ()

            # HTTP Response Core
            result = self.application(env, self.start_response)

            # Construct a response and send it back to the client
            self.finish_response(result)

    def parse_request(self, text):
        request_line = text.splitlines()[0]
        request_line = request_line.rstrip('\r\n')
        (self.request_method,  # GET
         self.path,            # /hello
         self.request_version  # HTTP/1.1
         ) = request_line.split()
        if (self.request_method == 'POST'):
            self.form_data = text.splitlines()[-1]
        else:
            self.form_data = ''

    def get_environ(self):
        env = {}
        # Send required variable to main HTTP function
        env['REQUEST_METHOD']    = self.request_method    # GET/POST/HEAD
        env['PATH_INFO']         = self.path              # /hello
        env['FORM_DATA']         = self.form_data         # key1=value1&key2=value2
        return env

    def start_response(self, status, response_headers):
        # Add necessary server headers
        server_headers = [
            ('Server', 'Progjar GG Serve'),
        ]

        # set header for response
        self.headers_set = [status, response_headers + server_headers]

    def finish_response(self, result):
        try:
            status, response_headers = self.headers_set
            response = 'HTTP/1.1 {status}\r\n'.format(status=status)

            for header in response_headers:
                response += '{0}: {1}\r\n'.format(*header)
            response += '\r\n'

            if (self.request_method != 'HEAD'):
                for data in result:
                    response += data
            
            # Print formatted response data a la 'curl -v'
            # print(''.join(
            #     '> {line}\n'.format(line=line)
            #     for line in response.splitlines()
            # ))
            
            self.client_connection.sendall(response)
        finally:
            self.client_connection.close()

# Function to open file
def getFileContents(location):
    fileDescriptor = open(location, 'rb')
    result = fileDescriptor.read()
    fileDescriptor.close()

    return result

# Here we code the main HTTP Handler
def statusHandler(environ, start_response):
    # Define default variable
    contentType = 'text/plain'
    status = ''
    responseData = []
    response_headers = [];
    interrupt = 0
    POST = {}
    if (environ['PATH_INFO'][-1] == '/'):
        environ['PATH_INFO'] = '/index.html'
        
    requestedFile = 'www/' + environ['PATH_INFO'][1:].split('?')[0]
    #check 403
    with open("config/403") as forbidden_list:
        counter = 0
        temp = requestedFile.strip()
        for data in forbidden_list:
            if(requestedFile == data.strip()):
                contentType = 'text/html'
                status = '404 FORBIDDEN'
                responseData = getFileContents('error_handler/403.html')
                interrupt = 1
                break
    #check 301
    with open("config/301") as moved_list:
        counter = 0
        for data in moved_list:
            data = data.strip()
            base, target = data.split("=>")
            if(requestedFile == base):
                status = '301 MOVED PERMANENTLY'
                contentType = 'text/html'
                responseData = getFileContents('error_handler/301.html')
                target = "http://localhost:8888/" + target
                response_headers.append(('Location', target))
                interrupt = 1
                break

    #if not affected by 301 or 403
    if (interrupt == 0):
        if (os.path.isfile(requestedFile)):
            status = '200 OK'
            responseData = getFileContents(requestedFile)
            
            fileExt = requestedFile.split('.')[-1]

            if (fileExt == 'htm') or (fileExt == 'html'):
                contentType = 'text/html'
            elif (fileExt == 'css'):
                contentType = 'text/css'
            elif (fileExt == 'js'):
                contentType = 'text/javascript'
            elif (fileExt == 'png'):
                contentType = 'image/png'
            elif (fileExt == 'gif'):
                contentType = 'image/gif'
            elif (fileExt == 'jpg') or (fileExt == 'jpeg'):
                contentType = 'image/jpeg'
            elif (fileExt == 'woff'):
                contentType = 'application/x-font-woff'
            elif (fileExt == 'woff2'):
                contentType = 'font/woff2'
            elif (fileExt == 'ttf'):
                contentType = 'application/x-font-ttf'

        else:
            contentType = 'text/html'
            status = '404 NOT FOUND'
            responseData = getFileContents('error_handler/404.html')

    if (environ['REQUEST_METHOD'] == 'POST'):
        formData = environ['FORM_DATA'].split('&')
        for inputData in formData: 
            splittedData=inputData.split('=')
            if len(splittedData)>1: 
                key, value=inputData.split('=')
                POST[key]=value
                print key + '=' + value

    response_headers.append(('Content-Type', contentType));

    start_response(status, response_headers)
    return responseData

if __name__ == "__main__":
    s = Server()
    s.run() 
